#ifndef __COMPLEX_H__
#define __COMPLEX_H__

typedef struct sComplex
{
  float real;
  float imag;
} tComplex, *pComplex;

#endif
