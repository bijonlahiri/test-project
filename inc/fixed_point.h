#ifndef __FIXED_POINT_H__
#define __FIXED_POINT_H__

#include "complex.h"

int float2fix(float input, int truncPos, int roundPos);
int cmplx2fix(tComplex input, int truncPos, int roundPos);
int *cmplx2fixArr(pComplex input, int truncPos, int roundPos, int numSamples);
float fix2float(int input, int truncPos, int roundPos);
tComplex fix2complex(int input, int truncPos, int roundPos);
pComplex fix2ComplexArr(int* input, int truncPos, int roundPos, int numSamples);
#endif
