#ifndef __SIGNAL_H__
#define __SIGNAL_H__

#include "complex.h"

pComplex signal_gen(float normFreq, int numSamples);
void write_signal_to_file(int *dataIn, int numSamples, FILE *outFile);
void read_signal_from_file(int *dataOut, int numSamples, FILE *inFile);
void print_signal(int *dataIn, int numSamples);

#endif
