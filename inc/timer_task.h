#ifndef __TIMER_TASK__H__
#define __TIMER_TASK__H__

#define LTE_SYM_TIME_US             1000

#define MAX_NUM_SYM_IN_SUBFRM       14
#define MAX_NUM_SUBFRM_IN_FRAME     10
#define MAX_NUM_FRAME               1000

#endif
