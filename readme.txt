
------------------------------------------Welcome to Bijon's & Sanket's Project---------------------------------------------
In this project, we will be learning programmin in 'C' Language and also learn the concepts of DSP

Get the repository by typing the following command

git clone https://bijonlahiri@bitbucket.org/bijonlahiri/test-project.git

Concept of Thread!
The concept of thread is introduced using the function thread_main.c
To run this function, download the repository, and then compile and run using a sinlge command "make thread"

Concept of Linked List!
The concept of linked list is introduced using the function linked_list.c
To run this function, download the repository, and then compile and run using a sinlge command "make ll"

QPSK Mapper!
8bit QPSK mapper introduction.
Run using "make qpsk" command.

Multi thread communication using pthread_cond_wait & MUTEX!
This is introduced in the files timer_task.c & timer_task.h
To run this part of the code, just use the command "make timer"

Generation of complex signals!
Included header files complex.h, signals.h, common_define.h
This demonstrates the generation of complex signals of specified frequencies, sampled by specified sampling frequency.
This creates a new file dump.txt in the folder where it is run.
To run this module, use the command "make signal"

Floating point to fixed point conversion!
The files fixed_point.c & fixed_point.h define the functions for converting float --> fixed point numbers.
These APIs are also used in the signal.c file for converting the signal generated to fixed point and storing in a file.
To run the fixed point piece of code, just use the command "make fix_point"

Thanks for dropping by!!!
