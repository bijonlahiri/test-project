#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "fixed_point.h"

#ifdef RUN_FIXED_POINT
int main(void)
{
  float input = -0.333333;
  tComplex cinput;
  tComplex coutput;
  cinput.real = 0.333333;
  cinput.imag = -0.333333;

  coutput = fix2complex(cmplx2fix(cinput, 16, 15), 16, 15);


  printf("0x%08X\n", float2fix(input, 16, 15));
  printf("0x%08X\n", cmplx2fix(cinput, 16, 15));

  printf("out_float  = %.6f\n", fix2float(float2fix(input, 16, 15), 16, 15));
  printf("out_real = %.6f\n", coutput.real);
  printf("out_imag = %.6f\n", coutput.imag);

  return 0;
}
#endif

/*
Float2Fix function, converts the input which is of type float to a fixed point output
The arguments roundPos, defines the fixed point fractional part
The arguments truncPos, defines what is the total bits of the output
*/
int float2fix(float input, int truncPos, int roundPos)
{
  int output;

  output = fabs(input) * pow(2, roundPos);

  //If input is less than 0, then perform two's complement of the output.
  if(input < 0)
  {
    output = ~output;
    output += 1;
  }

  output &= (1<<truncPos) - 1;

  return output;
}

int cmplx2fix(tComplex input, int truncPos, int roundPos)
{
  int output;

  output = float2fix(input.real, truncPos, roundPos);
  output <<= 16;
  output |= float2fix(input.imag, truncPos, roundPos);

  return output;
}

int *cmplx2fixArr(pComplex input, int truncPos, int roundPos, int numSamples)
{
  int *output = (int *)malloc(numSamples*sizeof(int *));
  for(int i=0; i<numSamples; i++)
  {
    output[i] = cmplx2fix(input[i], truncPos, roundPos);
  }

  return output;
}


 /*
 Fix2Float function, converts the input which is of type s(trucPos-(roundpos + 1)).roundPos to a floating output
 The arguments roundPos, defines the fixed point fractional part
 The arguments truncPos, defines what is the total bits of the input
 */
 float fix2float(int input, int truncPos, int roundPos)
 {
    float output;
    int new_input = input & ((1 << truncPos) - 1);
    //printf("0x%08X\n", new_input);
    int sign_bit  = input & (1 << (truncPos - 1));
   
   //If the sign bit  is 1, then perform two's complement of the output(negative number).
   if(sign_bit)
   {
     new_input  = ~new_input;
     //printf("0x%08X\n", new_input);
     new_input += 1;
     //printf("0x%08X\n", new_input);
     new_input = new_input & ((1 << truncPos) - 1) ;
     output = -((new_input) /  pow(2, roundPos));

   }
   else
   {
     output = (new_input) /  pow(2, roundPos);
   }
   return output;
}

tComplex fix2complex(int input, int truncPos, int roundPos)
 {
   tComplex output;
   int mask = (1 << truncPos) - 1;
   //printf("A0x%08X\n", input);
   int imag_input = (input & (mask << 0)) >> 0;
   //printf("I0x%08X\n", imag_input);
   int real_input = (input & (mask << 16)) >>16;
   //printf("R0x%08X\n", real_input); 

   output.real = fix2float(real_input, truncPos, roundPos);
   output.imag = fix2float(imag_input, truncPos, roundPos);
 
   return output;
 }


 pComplex fix2ComplexArr(int* input, int truncPos, int roundPos, int numSamples)
 {
   pComplex output = (pComplex)malloc(numSamples*sizeof(pComplex));
   for(int i=0; i<numSamples; i++)
   {
     output[i] = fix2complex(input[i], truncPos, roundPos);
   }

   return output;
 }

