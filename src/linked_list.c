#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// API Message Type Field coding definitions
#define MSG_TYPE_PHY_CONFIG_REQ       (0x1)
#define MSG_TYPE_PHY_CONFIG_RESP      (0x2)
#define MSG_TYPE_PHY_START_REQ        (0x3)
#define MSG_TYPE_PHY_START_RESP       (0x4)
#define MSG_TYPE_PHY_STOP_REQ         (0x5)
#define MSG_TYPE_PHY_STOP_RESP        (0x6)
#define MSG_TYPE_PHY_ERR_IND          (0x7)
#define MSG_TYPE_PHY_SHUTDOWN_REQ     (0x8)
#define MSG_TYPE_PHY_SHUTDOWN_RESP    (0x9)
//
#define MSG_TYPE_PHY_SLOT_IND         (0x10)
#define MSG_TYPE_PHY_DL_CONFIG_REQ    (0x11)
#define MSG_TYPE_PHY_UL_CONFIG_REQ    (0x12)
#define MSG_TYPE_PHY_UL_DCI_REQ       (0x13)
#define MSG_TYPE_PHY_TX_REQ           (0x14)
#define MSG_TYPE_PHY_CRC_IND          (0x15)
#define MSG_TYPE_PHY_RX_ULSCH_IND     (0x16)
#define MSG_TYPE_PHY_UCI_IND          (0x17)
#define MSG_TYPE_PHY_RX_RACH_IND      (0x18)
#define MSG_TYPE_PHY_RX_SRS_IND       (0x19)
#define MSG_TYPE_PHY_RX_ULSCH_UCI_IND (0x30)
//
// WLS operation with PDSCH Payload
#define MSG_PHY_ZBC_BLOCK0_REQ        (30)
#define MSG_PHY_ZBC_BLOCK1_REQ        (31)

// WLS operation with PUSCH Payload
#define MSG_PHY_ZBC_BLOCK_IND         (32)
//
#define MSG_TYPE_PHY_DL_IQ_SAMPLES          (200)
#define MSG_TYPE_PHY_UL_IQ_SAMPLES          (201)
#define MSG_TYPE_PHY_UL_UNCODED_BITS        (202)
#define MSG_TYPE_PHY_ADD_REMOVE_CORE        (203)
#define MSG_TYPE_PHY_UL_PRACH_IQ_SAMPLES    (204)


struct sLinkList
{
	char msgType[50];
	int msgNumber;
	char name[100];
	int age;
	float salary;
	char car[100];
	char house[5];
	
	struct sLinkList *pNext;
};

typedef struct sLinkList tLinkList;

void printList(tLinkList *head)
{
	tLinkList *currPos;
	currPos = head;

	while (currPos != NULL)
	{
		printf("\n");
        printf("msgType: %s\n", currPos->msgType);
        printf("msgNumber: %d\n", currPos->msgNumber);
		printf("Name: %s\n", currPos->name);
		printf("Age: %d\n", currPos->age);
		printf("Salary: %f\n", currPos->salary);
		printf("Car: %s\n", currPos->car);
		printf("House: %s\n", currPos->house);
		printf("\n");
		currPos = currPos->pNext;
	}
    free(currPos);
}

void push(tLinkList** HEAD ,tLinkList** NEW)
{
     (*NEW)->pNext = *HEAD;
     *HEAD = *NEW;

}

void pop(tLinkList** HEAD)
{
  if(*HEAD)
  {
   *HEAD = (*HEAD)->pNext;
  }
  else 
  {
   printf("Linklist is empty\n"); 
  }  
}
 


int main(void)
{
    //Actual head of linklist
	tLinkList *head = NULL;
	tLinkList *new;

    //initialise counter
    int counter = 0;
    
    //msg linked list head
    tLinkList *msg_head = NULL;


  while(0)
  {
    //declare a new linklist node to be pushed into linkedlist
    new = (tLinkList *)malloc(sizeof(tLinkList));
    
    //Take msgType
    printf("Enter  msgType:\n ");
	scanf("%s", new->msgType);
	fflush(stdin);
	
    counter++;
    new->msgNumber = counter;
 
    //Take Name
    printf("Enter person's Name:\n ");
    scanf("%s",new->name );
    fflush(stdin);

	//Take Age	
    printf("Enter Age of %s:\n ", new->name);
	scanf("%d", &(new->age));
	fflush(stdin);

    //Take Salary
    printf("Enter Salary of %s:\n ", new->name);
    scanf("%f", &(new->salary));
    fflush(stdin);
  
    //Take Car
    printf("Enter person's Car name:\n ");
    scanf("%s", new->car);
    fflush(stdin);

    //Take House
    printf("Enter person's House name:\n ");
    scanf("%s", new->house);
    fflush(stdin);

    //Push 
    push(&head,&new);

    //Free pointer and Break while loop
    if ( counter == 3)
    {
     printf("Currently in demo we are take total  %d nodes only \n",counter);
     free(new);
     break;
    }
  }//end of while
  
  int m;

  for(m = 1 ; m <= MSG_TYPE_PHY_RX_ULSCH_UCI_IND; m++)
  {
   new = (tLinkList *)malloc(sizeof(tLinkList));
   new->msgNumber = m;
   push(&msg_head,&new);
   if(m == MSG_TYPE_PHY_RX_ULSCH_UCI_IND)
     { 
      free(new);
     }
  }
 
  
  printList(msg_head);
  pop(&head);
  printList(head);
  return 0;
}
