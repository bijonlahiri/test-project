#include <stdio.h>
#include <stdlib.h>

typedef struct sComplex
{
  float real;
  float imag;
} tComplex, *pComplex;

pComplex qpsk_mapper(char input)
{
  tComplex mapperLut[4] = {{.real = 1, .imag = 1}, {.real = -1, .imag = 1}, {.real = -1, .imag = -1}, {.real = 1, .imag = -1}};
  char num = input;
  pComplex output = malloc(4*sizeof(tComplex));
  int bitPosition = 0;

  //printf("output: %p\n", &output[0]);

  for(int i=0; i<4; i++)
  {
    output[bitPosition] = mapperLut[(num & 0x3)];
    num >>= 2;
    bitPosition++;
  }

  return output;
}

int main(void)
{
  int num = 0x3;
  pComplex mapperOut;

  mapperOut = qpsk_mapper(num);

  //printf("mapperOut: %p\n", &mapperOut[0]);

  for(int i=0; i<4; i++)
  {
    printf("%f\t%f\n", mapperOut[i].real, mapperOut[i].imag);
  }

  return 0;
}
