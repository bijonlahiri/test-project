#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "complex.h"
#include "common_define.h"
#include "signal.h"
#include "fixed_point.h"

#ifdef RUN_SIGNAL
int main(void)
{
  float absFreq = 1000.0;
  float smplFreq = 4000.0;
  float normFreq = absFreq/smplFreq;
  int numCycles = 10;
  int numSamples = numCycles/normFreq;

  FILE *outFile = NULL; //fopen("./dump1.txt", "w");

  pComplex data = NULL;
  //pComplex readData = (pComplex)malloc(numSamples*sizeof(pComplex));
  int *fixPointData = NULL;

  data = signal_gen(normFreq, numSamples);
  fixPointData = cmplx2fixArr(data, 16, 14, numSamples);
  write_signal_to_file(fixPointData, numSamples, outFile);
  //read_signal_from_file(readData, numSamples, outFile);
  //print_signal(fixPointData, numSamples);
  return 0;
}
#endif

pComplex signal_gen(float normFreq, int numSamples)
{
  pComplex dataOut = (pComplex)malloc(numSamples*sizeof(pComplex));
  for(int i=0; i<numSamples; i++)
  {
    dataOut[i].real = cos(2*PI*normFreq*i);
    dataOut[i].imag = sin(2*PI*normFreq*i);
  }

  return dataOut;
}

void print_signal(int *dataIn, int numSamples)
{
  printf("-------------------%d IQ SAMPLES------------------\n", numSamples);

  for(int i=0; i<numSamples; i++)
  {
    printf("0x%X\n", dataIn[i]);
  }

}

void write_signal_to_file(int *dataIn, int numSamples, FILE *outFile)
{
  if(!outFile)
    outFile = fopen("./dump.txt", "w");

  for(int i=0; i<numSamples; i++)
  {
    //printf("%.5e\t%.5e\n", dataIn[i].real, dataIn[i].imag);
    fprintf(outFile, "0x%08X\n", dataIn[i]);
  }

  fclose(outFile);
}

void read_signal_from_file(int *dataOut, int numSamples, FILE *inFile)
{
  if(!inFile)
    inFile = fopen("./dump.txt", "r");

  for(int i=0; i<numSamples; i++)
  {
    fscanf(inFile, "0x%X", &(dataOut[i]));
    //printf("%.5e\t%.5e\n", dataOut[i].real, dataOut[i].imag);
  }

  fclose(inFile);
}
