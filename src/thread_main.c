  #include <stdio.h>
  #include <stdlib.h>
  #include <pthread.h>
  #include <unistd.h>

  #define MAX_STRING_LENGTH 100
  #define MAX_PERSONAL_COUNT 100

  struct s_personnel_data
  {
      char name[MAX_STRING_LENGTH];
      int age;
      double salary;
  };

  typedef struct s_personnel_data t_personnel_data;

  t_personnel_data g_personnel_data[MAX_PERSONAL_COUNT];
  int g_personnel_count = 0;
  int g_personnel_data_valid = 0;
  double g_avg_age = 0;
  double g_avg_salary = 0;

  void *get_personnel_data(void *threadid)
  {
      //size_t buf_size=100;
      //int *tId = (int *)threadid;
      printf("Starting thread: %d\n", *(int *)threadid);
      usleep(1000);
      while(1)
      {
          if (!g_personnel_data_valid)
          {
              printf("\nEnter personnel Name: ");
              fgets(&g_personnel_data[g_personnel_count].name[0], MAX_STRING_LENGTH, stdin);
              fflush(stdin);
              printf("Enter personnel age: ");
              scanf("%d", &g_personnel_data[g_personnel_count].age);
              fflush(stdin);
              printf("Enter personnel salary: ");
              scanf("%lf", &g_personnel_data[g_personnel_count].salary);
              fflush(stdin);
              g_personnel_count += 1;
              g_personnel_data_valid = 1;
          }
      }
      pthread_exit(NULL);
  }

  void *cal_personnel_data(void *threadid)
  {
      //int *tId = (int *)threadid;
      printf("Starting thread: %d\n", *(int *)threadid);
      usleep(1000);
      while(1)
      {
          if (g_personnel_data_valid)
          {
              double sum_age = 0;
              double sum_salary = 0;
              for (int i=0; i<g_personnel_count; i++)
              {
                  sum_age += g_personnel_data[i].age;
                  sum_salary += g_personnel_data[i].salary;
              }
              g_avg_age = sum_age/g_personnel_count;
              g_avg_salary = sum_salary/g_personnel_count;

              printf("\nNew personnel added '%s'\n", g_personnel_data[g_personnel_count-1].name);
              printf("AVG AGE: %lf\n", g_avg_age);
              printf("AVG SALARY: %lf\n", g_avg_salary);
              g_personnel_data_valid = 0;
          }
      }
      pthread_exit(NULL);
  }

  int main(void)
  {
      pthread_t thread1;
      pthread_t thread2;

      int tId1 = 0;
      int tId2 = 1;

      pthread_create(&thread1, NULL, get_personnel_data, (void *)&tId1);
      pthread_create(&thread2, NULL, cal_personnel_data, (void *)&tId2);

     pthread_exit(NULL);

  }
