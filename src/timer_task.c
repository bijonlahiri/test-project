#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include "timer_task.h"

typedef struct sTime
{
  unsigned int frameNum;
  unsigned int subFrmNum;
  unsigned int slotNum;
  unsigned int symNum;
  unsigned char trigTime;
} tTime;

static tTime gTime = {0};

pthread_cond_t condVar;
pthread_mutex_t mutexVar;

void *incr_time(void *data)
{
  tTime *timer;
  timer = (tTime *)data;
  while(1)
  {
    usleep(LTE_SYM_TIME_US);

    pthread_mutex_lock(&mutexVar);

    if(timer->symNum == (MAX_NUM_SYM_IN_SUBFRM-1))
    {
      if(timer->subFrmNum == (MAX_NUM_SUBFRM_IN_FRAME-1))
      {
        if(timer->frameNum == (MAX_NUM_FRAME-1))
          timer->frameNum = 0;
        else
          timer->frameNum++;

        timer->subFrmNum = 0;
      }
      else
        timer->subFrmNum++;

      timer->symNum = 0;
    }
    else
      timer->symNum++;

    //printf("(%d, %d, %d)\n", timer->frameNum, timer->subFrmNum, timer->symNum);
    timer->trigTime = 1;
    pthread_cond_signal(&condVar);
    pthread_mutex_unlock(&mutexVar);
  }
  pthread_exit(NULL);
}

void *file_thread(void *data)
{
  FILE *outFile;
  outFile = (FILE *)data;

  fprintf(outFile, "(%d, %d, %d)\n", gTime.frameNum, gTime.subFrmNum, gTime.symNum);

  fclose(outFile);

  pthread_exit(NULL);
}

void *init_tasks(void *data)
{
  tTime *timer;
  timer = (tTime *)data;

  pthread_t fileThread;

  FILE *outFile;

  while(1)
  {
    outFile = fopen("../dumps/timer_dump.txt", "a");
    pthread_mutex_lock(&mutexVar);
    while(!timer->trigTime)
      pthread_cond_wait(&condVar, &mutexVar);
    timer->trigTime = 0;
    pthread_mutex_unlock(&mutexVar);

    pthread_create(&fileThread, NULL, file_thread, (void *)outFile);
  }

  pthread_exit(NULL);
}

int main(void)
{
  pthread_t timerThread;
  pthread_t taskThread;

  if(!pthread_create(&timerThread, NULL, incr_time, (void *)&gTime))
    printf("SUCCESS: Timer thread created\n");
  if(!pthread_create(&taskThread, NULL, init_tasks, (void *)&gTime))
    printf("SUCCESS: Task thread created\n");

  pthread_exit(NULL);
  return 0;
}
