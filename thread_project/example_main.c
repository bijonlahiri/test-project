#include <stdio.h>
#include <stdlib.h>
#include <thread_pool.h>
#include <job_queue.h>
#include <unistd.h>
#include <pthread.h>

unsigned int taskBitmap = 0;
pthread_mutex_t gMutex = PTHREAD_MUTEX_INITIALIZER;

void func1(void* arg)
{
    int id = *(int*)arg;
    printf("func1[%d]\n", id);
    usleep(1000000);
    pthread_mutex_lock(&gMutex);
    taskBitmap |= 1<<id;
    pthread_mutex_unlock(&gMutex);

}

void func2(void* arg)
{
    int id = *(int*)arg;
    printf("func2[%d]\n", id);
    usleep(1000000);
    pthread_mutex_lock(&gMutex);
    taskBitmap |= 1<<id;
    pthread_mutex_unlock(&gMutex);
}

void func3(void* arg)
{
    int id = *(int*)arg;
    printf("func3[%d]\n", id);
    usleep(1000000);
    pthread_mutex_lock(&gMutex);
    taskBitmap |= 1<<id;
    pthread_mutex_unlock(&gMutex);
}

int main(void)
{
    static void (*function[6])(void*) = {func1, func2, func1, func1, func3, func2};
    int args[30];

    for(int i=0; i<30; i++)
      args[i] = i;

    threadPool* myThreadPool = thread_pool_init(3);
    for(int i=0; i<30; i++)
    {
        int j = i%6;
        job_queue_push(get_job_queue(myThreadPool), create_new_job(function[j], (void*)&args[i]));
    }
    //run_tasks(myThreadPool);
    while(taskBitmap != (1<<30)-1);
    thread_pool_destroy(myThreadPool);
    //usleep(100000);
    return 0;
}
