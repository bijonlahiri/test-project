#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

typedef void (*function)(void* arg);

typedef struct sJob
{
    struct sJob* nextJob;
    void (*function)(void* arg);
    void* args;
} job;

typedef struct sJobQueue
{
    pthread_mutex_t jobQueueMutex;
    job *front;
    job *rear;
    int len;
} jobQueue;

jobQueue* job_queue_init(void)
{
    jobQueue* newJobQueue = (jobQueue*)malloc(sizeof(jobQueue));

    newJobQueue->front = NULL;
    newJobQueue->rear = NULL;
    newJobQueue->len = 0;
    pthread_mutex_init(&newJobQueue->jobQueueMutex, NULL);
    return newJobQueue;
}

void job_queue_push(jobQueue* jobqueue, job* newjob)
{
    pthread_mutex_lock(&jobqueue->jobQueueMutex);

    switch(jobqueue->len)
    {
        case 0:
            jobqueue->front = newjob;
            jobqueue->rear = newjob;
            break;
        default:
            jobqueue->rear->nextJob = newjob;
            jobqueue->rear = newjob;
            break;
    }
    jobqueue->len++;
    //printf("Job queue len[%d]\n", jobqueue->len);
    pthread_mutex_unlock(&jobqueue->jobQueueMutex);
}

job* job_queue_pull(jobQueue* jobqueue)
{

    pthread_mutex_lock(&jobqueue->jobQueueMutex);
    job* nextJob = jobqueue->front;
    switch(jobqueue->len)
    {
        case 0:
            //printf("Job Queue Empty!\n");
            break;
        case 1:
            jobqueue->front = NULL;
            jobqueue->rear = NULL;
            jobqueue->len = 0;
            break;
        default:
            jobqueue->front = jobqueue->front->nextJob;
            jobqueue->len--;
            break;
    }

    pthread_mutex_unlock(&jobqueue->jobQueueMutex);

    return nextJob;
}

void job_queue_clear(jobQueue* jobqueue)
{

    while(jobqueue->len)
    {
        free(job_queue_pull(jobqueue));
    }

    jobqueue->front = NULL;
    jobqueue->rear = NULL;
    jobqueue->len = 0;
}

job* create_new_job(void (*function)(void*), void* args)
{
    job* newjob = (job*)malloc(sizeof(job));

    newjob->nextJob = NULL;
    newjob->function = function;
    newjob->args = args;

    return newjob;
}

function get_func(job* thisJob)
{
    return thisJob->function;
}

void* get_arg(job* thisJob)
{
    return thisJob->args;
}
