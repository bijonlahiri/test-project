#ifndef __JOB_QUEUE_H__
#define __JOB_QUEUE_H__

typedef struct sJob job;
typedef struct sJobQueue jobQueue;
typedef void (*function)(void*);
jobQueue* job_queue_init(void);
void job_queue_push(jobQueue* jobqueue, job* newjob);
job* job_queue_pull(jobQueue* jobqueue);
void job_queue_clear(jobQueue* jobqueue);
job* create_new_job(void (*function)(void*), void* args);
function get_func(job* thisJob);
void* get_arg(job* thisJob);
#endif
