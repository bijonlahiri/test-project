#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "job_queue.h"

typedef struct sThread
{
    int id;
    pthread_t thread;
} thread;

typedef struct sThreadPool
{
    thread** threads;
    pthread_mutex_t threadMutex;
    int num_threads_alive;
    int num_threads_working;
    int threads_run;
    int threads_execute;
    jobQueue* jobqueue;
} threadPool;

void* do_work(void* args)
{
    threadPool* threadpool = (threadPool*)args;
    pthread_mutex_lock(&threadpool->threadMutex);
    threadpool->num_threads_alive++;
    //printf("Num thread alive in thread[%d]\n", threadpool->num_threads_alive);
    pthread_mutex_unlock(&threadpool->threadMutex);

    while(threadpool->threads_run)
    {
      if(threadpool->threads_execute){
	    job* newjob = job_queue_pull(threadpool->jobqueue);
        //printf("newjob[%p]\n", newjob);
	    void (*function)(void*);
	    void* args;
	    if(newjob)
	    {
	        pthread_mutex_lock(&threadpool->threadMutex);
	        threadpool->num_threads_working++;
	        pthread_mutex_unlock(&threadpool->threadMutex);

	        function = get_func(newjob);
	        args = get_arg(newjob);

	        function(args);
	        //free(newjob);

	        pthread_mutex_lock(&threadpool->threadMutex);
	        threadpool->num_threads_working--;
	        pthread_mutex_unlock(&threadpool->threadMutex);
	    }
    }

    }

    pthread_mutex_lock(&threadpool->threadMutex);
    threadpool->num_threads_alive--;
    //printf("Thread killed!\n");
    pthread_mutex_unlock(&threadpool->threadMutex);

    return NULL;
}

thread* thread_init(int threadId, threadPool* threadpool)
{
    thread* newThread = (thread*)malloc(sizeof(thread));

    newThread->id = threadId;
    pthread_create(&newThread->thread, NULL, do_work, threadpool);
    pthread_detach(newThread->thread);
    //printf("Num threads alive[%d]\n", threadpool->num_threads_alive);
    return newThread;
}

threadPool* thread_pool_init(int numThread)
{
    threadPool* newThreadPool = (threadPool*)malloc(sizeof(threadPool));

    newThreadPool->num_threads_alive = 0;
    newThreadPool->num_threads_working = 0;
    newThreadPool->threads_run = 1;
    newThreadPool->threads_execute = 1;
    pthread_mutex_init(&newThreadPool->threadMutex, NULL);
    newThreadPool->jobqueue = job_queue_init();
    for(int i=0; i<numThread; i++)
    {
        newThreadPool->threads = (thread**)malloc(sizeof(thread));
        newThreadPool->threads[i] = thread_init(i, newThreadPool);
        //printf("Thread[%d] created\n", i);
    }
    while(newThreadPool->num_threads_alive != numThread);
    //printf("Num threads alive[%d]\n", newThreadPool->num_threads_alive);
    return newThreadPool;
}

void thread_pool_destroy(threadPool* threadpool)
{
    printf("Destroy thread pool!\n");
    threadpool->threads_run = 0;
    while(threadpool->num_threads_alive);
    job_queue_clear(threadpool->jobqueue);
    free(threadpool->jobqueue);
    free(threadpool->threads);
    free(threadpool);

}

jobQueue* get_job_queue(threadPool* thisThreadPool)
{
    return thisThreadPool->jobqueue;
}

void run_tasks(threadPool* thisThreadPool)
{
    thisThreadPool->threads_execute = 1;
}
