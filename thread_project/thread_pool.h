#ifndef __THREAD_POOL_H__
#define __THREAD_POOL_H__

#include "job_queue.h"

typedef struct sThread thread;
typedef struct sThreadPool threadPool;
void* do_work(void* args);
thread* thread_init(int threadId, threadPool* threadpool);
threadPool* thread_pool_init(int numThread);
void thread_pool_destroy(threadPool* threadpool);
jobQueue* get_job_queue(threadPool* thisThreadPool);
void run_tasks(threadPool* thisThreadPool);

#endif
